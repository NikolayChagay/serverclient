/**
 *  Stress tests
 * 
 * Стресс тесты моногопоточного сервера
 *
 *
**/

// local
#include "server/command_server.hpp"

// std
#include <iostream>
#include <cstring>
#include <math.h>
#include <vector>
#include <iomanip>
#include <functional>


using SimplePowServer = command::CommandServer<int()>;
class StressClient {
public:
  StressClient(SimplePowServer& server, const size_t& request_count):
    m_server(server),
    m_request_count(request_count),
    m_thread(std::ref(*this))
  { }

  ~StressClient() {
    if (m_thread.joinable())
      m_thread.join();
  }

  void operator()() {
    /// Выполняет команды возведения числа 2 в 10-ую степень
    /// Ждем выполнения не более 100 милисекунд
    for (size_t i = 0; i < m_request_count; ++i) {
      m_server.wait_for([] {  return std::pow(2, 10); }, std::chrono::milliseconds(100));
    }
  }

private:
  SimplePowServer& m_server;
  size_t m_request_count;
  std::thread m_thread;
};

auto Watcher = [](command::Statistics statistics) {
  std::cout << "Threads: " << std::setw(3) << statistics.active_workers
     << "  Busy threads: " <<  std::setw(3) << statistics.busy_workers
     << "  Idle threads: " << std::setw(3) << statistics.idle_workers
     << "  Commands in queue: " << std::setw(3) << statistics.total_waiting_commands
     << "  Commands processed: " << std::setw(3) << statistics.total_processed_commands
     << "  Timeout commands: " << std::setw(3) << statistics.total_invalid_commands
     << "  Error commands: " << std::setw(3) << statistics.total_failure_commands
     << std::endl;};

void test_01_01(SimplePowServer& server) {
  /// Число клиентских потоков = 1000, по 500 запросов в каждом
  std::vector<std::unique_ptr<StressClient>> clients;
  for (size_t i = 0; i < 1000; ++i)
    clients.emplace_back(std::make_unique<StressClient>(server, 500));
}

void test_01_02(SimplePowServer& server) {
  /// Число клиентских потоков = 5000, по 300 запросов в каждом
  std::vector<std::unique_ptr<StressClient>> clients;
  for (size_t i = 0; i < 5000; ++i)
    clients.emplace_back(std::make_unique<StressClient>(server, 300));
}

void test_01_03(SimplePowServer& server) {
  /// Число клиентских потоков = 10000, по 200 запросов в каждом
  std::vector<std::unique_ptr<StressClient>> clients;
  for (size_t i = 0; i < 10000; ++i)
    clients.emplace_back(std::make_unique<StressClient>(server, 200));
}

void test_01() {
    std::cout << "=== Сreate a command server and try to spam it with a variable load  ===" << std::endl;
    command::CommandServer<int()> server(20, 60, std::chrono::seconds(5));
    server.watch_statistics(Watcher, std::chrono::seconds(1));

    /// step 0
    server.run();
    std::this_thread::sleep_for(std::chrono::seconds(1));

    /// step 1
    std::cout << "The start of the spam server>>>>" << std::endl;

    test_01_01(server);

    std::cout << "Going to sleep for 20 seconds and wait for the pool to compress" << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(10));

    /// step 2
    std::cout << "Start spamming server again" << std::endl;

    test_01_02(server);

    std::cout << "Going to sleep for 20 seconds and wait for the pool to compress" << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(10));

    /// step 3
    std::cout << "Start spamming server one more time" << std::endl;

    test_01_03(server);

    std::cout << "<<<<< The end of the spam server" << std::endl;

    server.stop();
}

int main (int argc, char* argv[] )
{
  test_01();

  return 0;
}

