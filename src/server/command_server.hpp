/**
 *  Multithreads server
 * 
 * Класс моногопоточного сервера
 *
 *
**/
#pragma once

// local
#include "detail/worker_pool.hpp"

namespace command {

  /// Statistics
  using Statistics = detail::Statistics;

  /// CommandServer
  template <typename T> class CommandServer;

  /// CommandServer
  template <typename R, typename ...Args>
  class CommandServer<R(Args...)> {
  public:
    using Function = R(Args...);

    CommandServer(
      /// Минимальное количество запущенных обработчиков команд
      const size_t& lower,
      /// Лимит на количество запущенных обработчиков команд
      const size_t& upper,
      /// Максимальное время простоя обработчика (сек.)
      const std::chrono::seconds& unused_ttl):
     m_stoped(false),
     m_lower(lower),
     m_upper(upper),
     m_unused_ttl(unused_ttl)
   { }

    ~CommandServer() { stop_wait(); }

    CommandServer() = delete;
    CommandServer(const CommandServer&) = delete;
    CommandServer(CommandServer&&) = delete;
    CommandServer &operator=(const CommandServer&) = delete;
    CommandServer &operator=(CommandServer&&) = delete;

    /// Запуск
    void run() {
      m_pool = std::make_unique<detail::WorkerPool<Function>>(m_lower, m_upper, m_unused_ttl, m_queue, m_stat);
      m_thread = std::make_unique<std::thread>(std::ref(*this));
    }

    /// Факт остановки сервера
    bool stoped() const {
      std::lock_guard<std::mutex> lock(m_mutex);
      return m_stoped;
    }

    /// Инициация остановки потока выполнения сервера
    void stop() {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_stoped = true;
    }

    /// Остановить поток выполнения сервера с ожиданием остановки
    void stop_wait() noexcept {
      try {
        stop();
        if (m_thread && m_thread->joinable())
          m_thread->join();
      }
      catch(...) { }
    }

    /// Добавить команду в очередь
    template<typename Lambda, typename Sla>
    CommandServer& add_command(Lambda&& lambda, Sla&& sla) {
      m_queue.add_command(std::move(lambda), std::move(sla));
      return *this;
    }

    /// Выполнение команды с ожиданием
    template<typename Lambda, typename Sla>
    std::future<R> wait_for(Lambda&& lambda, Sla&& ms) {
      auto command{m_queue.add_command(std::move(lambda), std::move(ms))};
      auto f = command->task().get_future();
      f.wait_for(ms);
      return f;
    }

    /// Обработчик основного потока сервера
    void operator()() {
      if (!m_pool)
        throw std::runtime_error("Command server is not running.");

      while (!stoped()) {
        // Выделение необходимых ресурсов, для обработки команд
        m_pool->create_worker_if_needed();

        // Пометить просроченные обработчики к удалению
        m_pool->stop_expired_workers();

        // Удалить, просроченные, неиспользуемые обработчики (сжать пул)
        m_pool->remove_expired_workers();

        // Фиксация статистики
        call_statistics();

        // При отсутствии команд - поток ненадолго засыпает
        if (!m_queue.have_commands())
          std::this_thread::sleep_for(std::chrono::milliseconds(1));
      }

      // Остановить все обработчики с ожиданием
      m_pool->all_stop_wait();
    }

    /// Регистрация клиентского обработчика, для сбора статистики
    /// Одновременно, регистрируется только один обработчик
    using WatchCallback = std::function<void(Statistics)>;
    void watch_statistics(WatchCallback&& watcher, const std::chrono::seconds& period) {
      auto now_time{std::chrono::steady_clock::now()};
      m_watch = {std::move(watcher), period, std::move(now_time)};
      detail::StatScopedLocker s(m_stat);
      auto& statistics  = s.statistics();
      statistics = Statistics();
    }

  private:

    /// Фиксация статистики
    inline void call_statistics() {
      if (m_watch.callback) {
        auto now_time{std::chrono::steady_clock::now()};
        auto d{std::chrono::duration_cast<std::chrono::seconds>(now_time - m_watch.start_point)};
        if (d > m_watch.period) {
          m_watch.start_point = now_time;
          detail::StatScopedLocker s(m_stat);
          auto& statistics  = s.statistics();
          statistics.active_workers = m_pool->alive_size();
          statistics.busy_workers = m_pool->busy_size();
          statistics.idle_workers = statistics.active_workers - statistics.busy_workers;
          statistics.total_waiting_commands = m_queue.size();
          m_watch.callback(statistics);
        }
      }
    }

  private:

    /// Флаг остановки сервера
    bool m_stoped;

    /// Минимальное число одновременно
    /// запущенных потоков вполнения команд
    size_t m_lower;

    /// максимальное число обновременно
    /// запущенных потоков выполнения команд
    size_t m_upper;

    /// Максимальное время простоя потока, когда он не выполняет команды
    std::chrono::seconds m_unused_ttl;

    /// Очередь команд
    detail::CommandQueue<Function> m_queue;

    /// Пул потоков-обработчиков команд
    std::unique_ptr<detail::WorkerPool<Function>> m_pool;

    /// Поток выполнения основного цикла сервера
    std::unique_ptr<std::thread> m_thread;

    /// Защита данных сервера в многопоточной среде
    mutable std::mutex m_mutex;

    /// Поддержка клиентского мониторинга статистики
    struct {
      WatchCallback callback;
      std::chrono::seconds period;
      std::chrono::time_point<std::chrono::steady_clock> start_point;
    } m_watch;

    /// Собранные данные мониторинга статистики
    detail::Stat m_stat;

  };

} // namespace command

