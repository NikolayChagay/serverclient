/**
 * Command Queue
 * 
 * Очередь заданий сервера обработки команд
 *
**/
#pragma once

// std
#include <list>
#include <chrono>
#include <mutex>
#include <memory>
#include <deque>
#include <future>
#include <numeric>

namespace command::detail {

  /// Описание команды в очереди
  template <typename Function>
  class Command
  {
  public:

    template <typename Lambda, typename Sla>
    explicit Command(Lambda&& lambda, Sla&& sla):
      m_task(std::move(lambda)),
      m_task_sla(std::move(sla)),
      m_add_time(std::chrono::steady_clock::now()),
      m_invalid(false)
    { }

   /// Доступ к задаче
   std::packaged_task<Function>& task()
   { return m_task; }

   bool invalid()
   {
       auto now_time{std::chrono::steady_clock::now()};
       auto d{std::chrono::duration_cast<std::chrono::seconds>(now_time - m_add_time)};
       if (d > m_task_sla) {
           m_invalid = true;
       };

       return m_invalid;
   }

  private:
    /// Обработчик команды
    std::packaged_task<Function> m_task;

    /// SLA
    std::chrono::milliseconds m_task_sla;

    /// Время добавления в очередь
    std::chrono::steady_clock::time_point m_add_time;

    /// Статус задания
    bool m_invalid;
  };

  /// CommandQueue
  template <typename Function>
  class CommandQueue {
  public:
    using CommandPtr = std::shared_ptr<Command<Function>>;

    /// Запрос следующей команды из очереди
    CommandPtr get_next() {
      std::lock_guard<std::mutex> lock(m_mutex);

      if (m_queue.empty())
        return nullptr;

      auto command{m_queue.front()};
      m_queue.pop_front();
      return command;
    }

    /// Добавить команду в очередь на исполнение
    template<typename Lambda, typename Sla>
    inline CommandPtr add_command(Lambda&& lambda, Sla&& sla) {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_queue.emplace_back(std::make_shared<Command<Function>>(std::move(lambda), std::move(sla)));
      return m_queue.back();
    }

    /// Факт наличия команд в очереди
    inline bool have_commands() const {
      std::lock_guard<std::mutex> lock(m_mutex);
      return !m_queue.empty();
    }

    /// Размер очереди команд
    inline size_t size() const {
      std::lock_guard<std::mutex> lock(m_mutex);
      return m_queue.size();
    }

    /// Очистить очередь команд
    void clear() {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_queue.clear();
    }

  private:
    /// Очередь команд
    std::deque<CommandPtr> m_queue;

    ///  Защита данных очереди в многопоточной среде
    mutable std::mutex m_mutex;
  };
} // namespace command::detail
