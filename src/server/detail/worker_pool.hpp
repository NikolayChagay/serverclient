/**
 *  Worker Pool
 * 
 * Класс пула потоков
 *
 *
**/
#pragma once

// local
#include "command_queue.hpp"
#include "statistics.hpp"

// std
#include <thread>
#include <stdexcept>
#include <list>
#include <algorithm>
#include <numeric>

namespace command::detail {

  /// WorkerPool
  template <typename Function> class WorkerPool;

  /// Worker
  template<typename Function>
  class Worker {
  public:

    /// Текущий статус потока-обработчика команд
    enum class Status { wait, busy };

    Worker(CommandQueue<Function>& queue,
           WorkerPool<Function>& pool):
      m_queue(queue),
      m_pool(pool),
      m_stoped(false),
      m_status(Status::wait),
      m_thread(std::ref(*this))
    { }

    ~Worker() { stop_wait(); }

    /// Остановка потока-обработчика с ожиданием
    void stop_wait() noexcept {
      try {
        stop();
        if (m_thread.joinable())
          m_thread.join();
      }
      catch(...) { }
    }

    /// Факт инициации остановки потока-обработчкика
    inline bool stoped() const {
      std::lock_guard<std::mutex> lock(m_mutex);
      return m_stoped;
    }

    /// Инициировать остановку обработчика
    inline void stop() {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_stoped = true;
    }

    /// Текущий статус обработчкика
    inline Status status() const {
      std::lock_guard<std::mutex> lock(m_mutex);
      return m_status;
    }

    /// Обработчик занят выполнением команды
    inline bool busy() const {
      return alive() && Status::busy == status();
    }

    /// Обработчик активен и может принимать задачи для обработки
    inline bool alive() const {
      return m_thread.joinable() && !stoped();
    }

    /// Временная точка последней активности обработчика
    inline std::chrono::time_point<std::chrono::steady_clock> last_activity() const {
      std::lock_guard<std::mutex> lock(m_mutex);
      return m_last_activity;
    }

    /// Основной цикл обработчика
    void operator()() {
      update_activity();
      while (!stoped()) {
        auto next_task{m_queue.get_next()};

        if (!next_task) {
          update_status(Status::wait);
          std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
        else if (next_task->invalid()) {
          StatScopedLocker s(m_pool.stat());
          ++s.statistics().total_invalid_commands;
        }
        else {
          update_status(Status::busy);
          update_activity();
          try {
            next_task->task()();
            StatScopedLocker s(m_pool.stat());
            ++s.statistics().total_processed_commands;
          }
          catch(...) {
            StatScopedLocker s(m_pool.stat());
            ++s.statistics().total_failure_commands;
          }
        }
      }
    }

  private:

    /// Актуализировать временную метку активности обработчика
    inline void update_activity() {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_last_activity = std::chrono::steady_clock::now();
    }

    /// Обновить текущий статус обработчика
    inline void update_status(Status status) {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_status = status;
    }

  private:
    /// Доступ к очереди команд
    CommandQueue<Function>& m_queue;

    /// Доступ к родительскому пулу обработчиков
    WorkerPool<Function>& m_pool;

    /// Флаг остановки потока-обработчкика
    bool m_stoped;

    /// Текущий статус потока-обработчика
    Status m_status;

    /// Защита данных обработчика в много-поточной среде
    mutable std::mutex m_mutex;

    /// Поток выполнения команд обработчика
    std::thread m_thread;

    /// Временная метка последней активности обработчика
    std::chrono::time_point<std::chrono::steady_clock> m_last_activity;
  };

  /// WorkerPool
  template <typename Function>
  class WorkerPool {
  public:
    WorkerPool(const size_t& lower, const size_t& upper,
               const std::chrono::seconds& unused_ttl,
               CommandQueue<Function>& queue,
               Stat& stat):
      m_lower(lower),
      m_upper(upper),
      m_unused_ttl(unused_ttl),
      m_queue(queue),
      m_stat(stat)
    {
      if (lower > upper)
        throw std::runtime_error("Incorrect bounds of initialization.");
    }

    ~WorkerPool() { all_stop_wait(); }

    /// Доступ к минимальному значению числа активных обработчиков
    const size_t& lower() const { return m_lower; }

    /// Доступ к максимальному значению числа активных обработчиков
    const size_t& upper() const { return m_upper; }

    /// Доступ к значению максимального интервала времени простоя обработчика
    const std::chrono::seconds& unused_ttl() { return m_unused_ttl; }

    /// Число активных обработчиков
    const size_t alive_size() const {
      std::lock_guard<std::mutex> lock(m_mutex);
      return std::accumulate(m_pool.begin(), m_pool.end(), 0,
        [](int size, const WorkerPtr& w) { return w->alive() ? 1 + size : size; });
    }

    /// Число занятых обработчиков
    const size_t busy_size() const {
      std::lock_guard<std::mutex> lock(m_mutex);
      return std::accumulate(m_pool.begin(), m_pool.end(), 0,
        [](int size, const WorkerPtr& w) { return w->busy() ? 1 + size : size; });
    }

    using WorkerPtr = std::unique_ptr<Worker<Function>>;

    /// Удалить из пула остановленные обработчики, не имеющие потока выполнения
    inline void remove_expired_workers() {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_pool.remove_if([](const WorkerPtr& w) -> bool { return w->stoped(); }); 
    }

    /// Все активные обработчики, заняты выполнением команд
    inline bool all_busy() const {
      std::lock_guard<std::mutex> lock(m_mutex);
      return std::all_of(m_pool.begin(), m_pool.end(),
       [](const WorkerPtr& w) -> bool { return w->busy(); });
    }

    /// Создание нового обработчика, с проверкой на максимально -возможное количество обработчиков
    inline void try_create_worker() {
      if (alive_size() >= m_upper)
        throw std::runtime_error("Worker limit exceeded.");
      std::lock_guard<std::mutex> lock(m_mutex);
      m_pool.emplace_back(create_worker());
    }

    /// Логика создания новых обработчиков, по ситуативным условиям.
    inline void create_worker_if_needed() {
      // Обеспечение требования к числу обработчиков по нижней границе
      while (alive_size() < m_lower)
        try_create_worker();

      // Обеспечение требования к числу обработчиков по верхней границе
      // ( создать новый, если текущие не справляются)
      // Все обработчики заняты выполнением команд, и их количество не превышает верхний лимит
      if (all_busy() && alive_size() < m_upper)
         try_create_worker();
    }

    /// Остановка всех обработчков с ожиданием
    inline void all_stop_wait() noexcept {
      std::lock_guard<std::mutex> lock(m_mutex);
      for (auto& w : m_pool )
        w->stop_wait();
      m_pool.clear();
    }

    /// Инициирование процесса остановки просроченных обработчиков
    inline void stop_expired_workers() {
      auto now_time{std::chrono::steady_clock::now()};
      auto alive_sz{alive_size()};
      std::lock_guard<std::mutex> lock(m_mutex);

      for (auto& w : m_pool) {
        // Число секунд, с момента последней активности обработчика
        auto d{std::chrono::duration_cast<std::chrono::seconds>(now_time - w->last_activity())};
        // Обработчик просрочен, и его удаление не нарушит нижнюю границу количества обработчиков
        if (d > unused_ttl() && alive_sz > m_lower)
          w->stop();
      }
    }

    /// Доступ к статистике
    Stat& stat() { return m_stat; }

  private:
   /// Создание нового экземпляра обработчика
   inline WorkerPtr create_worker() { return std::make_unique<Worker<Function>>(m_queue, *this); }

  private:
    /// Минимальное число одновременно
    /// запущенных потоков вполнения команд
    const size_t& m_lower;

    /// максимальное число обновременно
    /// запущенных потоков выполнения команд
    const size_t& m_upper;

    /// Максимальное время простоя потока, когда он не выполняет команды
    const std::chrono::seconds& m_unused_ttl;

    /// Доступ к очереди команд
    CommandQueue<Function>& m_queue;

    /// Пул обработчкиов
    std::list<WorkerPtr> m_pool;

    /// Защита данных пула, в многопоточной среде
    mutable std::mutex m_mutex;
 
    /// Данные статистики
    Stat& m_stat;
 };

} // namespace command::detail
