/**
 * Statistics
 * 
 * Cтатистика сервера обработки команд
 *
**/
#pragma once

// std
#include <mutex>

namespace command::detail {

  /// Statistics
  struct Statistics {
    /// Количество обработчиков
    size_t active_workers = 0;

    /// Количество занятых обработчиков
    size_t busy_workers = 0;

    /// Количество простаивающих обработчиков
    size_t idle_workers = 0;

    /// Количество команд в очереди, ожидающих выполнения
    size_t total_waiting_commands = 0;

    /// Количество уже выполненных команд очереди
    size_t total_processed_commands = 0;

    /// Количество команд, с ошибками
    size_t total_failure_commands = 0;

    /// Количество инвалидных задач (по таймауту)
    size_t total_invalid_commands = 0;
  };

  // StatScopedLocker
  class StatScopedLocker;

  /// Stat
  class Stat {
  public:
    friend class StatScopedLocker;
    Stat(): m_statistics()
    { }
    Statistics& statistics() { return m_statistics; }

  private:
    /// Данные статистики
    Statistics m_statistics;

    /// Защита данных статистики, в многопоточной среде
    mutable std::mutex m_mutex;
  };

  // StatScopedLocker
  class StatScopedLocker {
  public:
    StatScopedLocker(Stat& stat):
      m_stat(stat)
   { m_stat.m_mutex.lock(); }
   ~StatScopedLocker() { m_stat.m_mutex.unlock(); }

   Statistics& statistics() { return m_stat.statistics(); }

  private:
    Stat& m_stat;
  };

} /// command::detail 
